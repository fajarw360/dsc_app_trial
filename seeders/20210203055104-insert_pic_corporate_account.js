'use strict';
const bcrypt = require('bcrypt');
const pass = 'Test1234'
const saltRounds = 10;
const passdefault = bcrypt.hashSync(pass, saltRounds);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('pic_corporate_accounts', 
    [
      {
        name: 'Budi Mandiri',
        password: passdefault,
        nik: '317501',
        email: 'budi@mandiri.co.id',
        phone: '628118601111',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Bayu BCA',
        password: passdefault,
        nik: '317502',
        email: 'bayu@bca.co.id',
        phone: '628111881111',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Bagus Cocacola',
        password: passdefault,
        nik: '317503',
        email: 'bagus@cocacola.co.id',
        phone: '62811221111',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Andi Unilever',
        password: passdefault,
        nik: '317504',
        email: 'andi@unilever.co.id',
        phone: '628118111111',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Angga Pertamina',
        password: passdefault,
        nik: '317505',
        email: 'angga@pertamina.co.id',
        phone: '628114901111',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Dimas Garuda',
        password: passdefault,
        nik: '317506',
        email: 'dimas@garuda.co.id',
        phone: '62811711111',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Ajeng JNE',
        password: passdefault,
        nik: '317507',
        email: 'ajeng@jne.co.id',
        phone: '628118602222',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Adit SiCepat',
        password: passdefault,
        nik: '317508',
        email: 'adit@sicepat.co.id',
        phone: '628111882222',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Dwi SAP Express',
        password: passdefault,
        nik: '317509',
        email: 'dwi@sapexpress.co.id',
        phone: '62811222222',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Andi Kompas',
        password: passdefault,
        nik: '317510',
        email: 'andi@kompas.co.id',
        phone: '628118112222',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Angga PGN',
        password: passdefault,
        nik: '317511',
        email: 'angga@pgn.co.id',
        phone: '628114902222',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Dimas Elnusa',
        password: passdefault,
        nik: '317512',
        email: 'dimas@elnusa.co.id',
        phone: '62811712222',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Ajeng PLN',
        password: passdefault,
        nik: '317513',
        email: 'ajeng@pln.co.id',
        phone: '628118603333',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Adit Waskita',
        password: passdefault,
        nik: '317514',
        email: 'adit@waskita.co.id',
        phone: '628111883333',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Dwi JW Marriot',
        password: passdefault,
        nik: '317515',
        email: 'dwi@jwmarriot.co.id',
        phone: '62811223333',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Andi Indomarco',
        password: passdefault,
        nik: '317516',
        email: 'andi@indomarco.co.id',
        phone: '628118113333',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Angga Freeport',
        password: passdefault,
        nik: '317517',
        email: 'angga@freeport.co.id',
        phone: '628114903333',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Dimas BP Indonesia',
        password: passdefault,
        nik: '317518',
        email: 'dimas@bpindonesia.co.id',
        phone: '62811713333',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('pic_corporate_accounts', null, {});
  }
};

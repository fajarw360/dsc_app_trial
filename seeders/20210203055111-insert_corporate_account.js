'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('corporate_accounts', 
    [
      {
        account_name: 'PT Bank Mandiri (Persero) Tbk',
        address:'Jakarta',
        line_of_business:'Banking',
        phone:6214121111,
        pic_id:1,
        am_id:1,
        virtual_account: '122341111111',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'Bank Central Asia Tbk.',
        address:'Jakarta',
        line_of_business:'Banking',
        phone:6215121111,
        pic_id:2,
        am_id:1,
        virtual_account: '122341111112',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'COCA COLA AMATIL',
        address:'Jakarta',
        line_of_business:'Manufacturing',
        phone:6216121111,
        pic_id:3,
        am_id:1,
        virtual_account: '122341111113',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'Unilever Indonesia',
        address:'Jakarta',
        line_of_business:'Manufacturing',
        phone:6217121111,
        pic_id:4,
        am_id:2,
        virtual_account: '122341111114',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT Pertamina',
        address:'Jakarta',
        line_of_business:'Oil & Mining',
        phone:6218121111,
        pic_id:5,
        am_id:2,
        virtual_account: '122341111115',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT Garuda Indonesia Tbk',
        address:'Jakarta',
        line_of_business:'Utilities & Public Service',
        phone:6219121111,
        pic_id:6,
        am_id:2,
        virtual_account: '122341111116',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT TIKI JNE',
        address:'Jakarta',
        line_of_business:'Logistics & Distribution',
        phone:6214122222,
        pic_id:7,
        am_id:3,
        virtual_account: '122341111117',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'SiCepat Express Indonesia',
        address:'Jakarta',
        line_of_business:'Logistics & Distribution',
        phone:6215122222,
        pic_id:8,
        am_id:3,
        virtual_account: '122341111118',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'SATRIA ANTARAN PRIMA',
        address:'Jakarta',
        line_of_business:'Logistics & Distribution',
        phone:6216122222,
        pic_id:9,
        am_id:3,
        virtual_account: '122341111119',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT KOMPAS MEDIA NUSANTARA',
        address:'Jakarta',
        line_of_business:'Media',
        phone:6217122222,
        pic_id:10,
        am_id:4,
        virtual_account: '122341111120',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'Perusahaan Gas Negara (Persero) Tbk.',
        address:'Jakarta',
        line_of_business:'Utilities',
        phone:6218122222,
        pic_id:11,
        am_id:4,
        virtual_account: '122341111121',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT ELNUSA TBK',
        address:'Jakarta',
        line_of_business:'Oil & Mining',
        phone:6219122222,
        pic_id:12,
        am_id:4,
        virtual_account: '122341111122',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT PLN (Persero)',
        address:'Jakarta',
        line_of_business:'Utilities',
        phone:6214123333,
        pic_id:13,
        am_id:5,
        virtual_account: '122341111123',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT WASKITA KARYA (PERSERO) TBK',
        address:'Jakarta',
        line_of_business:'Public Service',
        phone:6215123333,
        pic_id:14,
        am_id:5,
        virtual_account: '122341111124',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'JW MARRIOTT HOTEL',
        address:'Jakarta',
        line_of_business:'Hospitality',
        phone:6216123333,
        pic_id:15,
        am_id:5,
        virtual_account: '122341111125',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        account_name: 'PT INDOMARCO PRISMATAMA',
        address:'Jakarta',
        line_of_business:'Retail',
        phone:6217123333,
        pic_id:16,
        am_id:6,
        virtual_account: '122341111126',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Angga Freeport',
        password: passdefault,
        nik: '317517',
        email: 'angga@freeport.co.id',
        phone: '628114903333',
        job_position: 'IT Service Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        account_name: 'PT Freeport Indonesia',
        address:'Jakarta',
        line_of_business:'Oil & Mining',
        phone:6218123333,
        pic_id:17,
        am_id:5,
        virtual_account: '122341111126',
        logo:'aaaa',
        is_active:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Dimas BP Indonesia',
        password: passdefault,
        nik: '317518',
        email: 'dimas@bpindonesia.co.id',
        phone: '62811713333',
        job_position: 'IT Operational Manager',
        workplace: 'Jakarta',
        avatar: 'aaaaa',
        is_active: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('corporate_accounts', null, {});
  }
};

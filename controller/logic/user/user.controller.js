const jwt = require('jsonwebtoken');
// const { pic_corporate_account } = require('../../../models')
const { account_manager } = require('../../../models')
const {
    hashSync,
    genSaltSync,
    compareSync
} = require("bcrypt");

class UserController {
    async editProfileAM(req, res, next) {
        const salt = genSaltSync(10);
        const hashedPassword = hashSync(req.body.password, salt);
        const { name, nik, email, password, phone, job_position, workplace, avatar } = req.body
        console.log(req.body);
        
        if (req.businessLogic != undefined) {
            next()
            return
        }
        if (name == undefined || nik == undefined ||  email == undefined || password == undefined  || phone == undefined || job_position == undefined || workplace == undefined
            || name == "" || nik == "" || email == "" || password == "" || phone == "" || job_position == "" || workplace == "" ) {
                console.log('state gagal')
                res.status(401).json({
                    "status": false,
                    "message": "Column Not Complete",
                    "data": null
            })
            next()
            return
        }
        await account_manager.findAll({ where: { email:email} })
            .then(async data => {
                if (data.length > 0) {
                    await account_manager.update({
                        name: name,
                        nik:nik,
                        // email: email,
                        password: hashedPassword,
                        phone: phone,
                        job_position:job_position,
                        workplace:workplace,
                        avatar: avatar,
                    }, {
                            where: {
                                email:email
                            }
                        }).then(
                            async data => {
                                res.status(200).json({
                                    "status": true,
                                    "message": "Update Profile Success",
                                    "data": { 
                                        "name": name,
                                        "nik" :nik,
                                        "email": email,
                                        "password": hashedPassword,
                                        "phone": phone,
                                        "job_position":job_position,
                                        "workplace":workplace,
                                        "avatar": avatar
                                     }
                                })
                                next()
                                return
                            }
                        ).catch(
                            async err => {
                                console.log(err)
                                res.status(401).json({
                                    "status": false,
                                    "message": "Update Profile Error",
                                    "data": null
                                })
                                next()
                                return
                            }
                        )
                } else {
                    res.status(401).json({
                        "status": false,
                        "message": "Update Profile error",
                        "data": null
                    })
                    next()
                    return
                }
            })
            .catch(async err => {
                console.log(err)
                res.status(500).json({
                    "status": false,
                    "message": err,
                    "data": null
                })
                next()
                return
            })
    }

}

module.exports = UserController
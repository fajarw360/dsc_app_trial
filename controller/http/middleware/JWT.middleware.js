const jwt = require('jsonwebtoken');

module.exports = {
    JWTMiddleware: (req, res, next) => {
        try{
        const authHeader = req.header('Authorization')
        if (authHeader.indexOf("Bearer ") >= 0) {
            const token = authHeader.split("Bearer ")
            var decoded = jwt.verify(token[1], process.env.SECRET)
            req.email = decoded
            console.log(req.email)
            next()
        } else {
            const bodyRes = {
                status:"Failed",
                message: "Unauthorized"
            }
            res.send(bodyRes)
            return
        }
    } catch(error) {
        console.log(error)
        if (error.name === 'TypeError') {
            const bodyRes = {
                status: "Failed",
                message: "Unauthorized"
              }
            res.send(bodyRes)
            return 
        }
        if (error.name === 'TokenExpiredError') {
            const bodyRes = {
                message: "Token Expired"
              }
            res.send(bodyRes) 
            return
        }
        const bodyRes = {
            message: "Internal Server Error"
        }
        res.send(bodyRes)
        return
    }
    }
}
var express = require('express');
var router = express.Router();
const { apikeyMiddleware } = require('../controller/http/middleware/apikey.middleware');
const { JWTMiddleware } = require('../controller/http/middleware/JWT.middleware');

const authController = require('../controller/logic/auth/auth.controller')
const userController = require('../controller/logic/user/user.controller')


const ac = new authController
const uc = new userController

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
//Login Corporate
router.post('/logincorporate', apikeyMiddleware, ac.authCorporate)
//login AM
router.post('/logintelkomsel', apikeyMiddleware, ac.authAM)

//edit profile AM
router.post('/editprofiletelkomsel', JWTMiddleware, apikeyMiddleware, uc.editProfileAM)


module.exports = router;

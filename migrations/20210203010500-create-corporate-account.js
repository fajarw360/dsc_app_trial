'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('corporate_accounts', {
      account_id: {
        type: Sequelize.INTEGER
      },
      account_name: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.STRING
      },
      line_of_business: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING
      },
      pic_id: {
        type: Sequelize.INTEGER
      },
      am_id: {
        type: Sequelize.INTEGER
      },
      virtual_account:{
        type: Sequelize.STRING
      },
      logo: {
        type: Sequelize.BLOB
      },
      is_active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('corporate_accounts');
  }
};
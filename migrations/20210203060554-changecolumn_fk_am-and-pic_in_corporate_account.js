'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn( 
      'corporate_accounts', 
      'am_id',
      {
       type : Sequelize.INTEGER,
       references: {
         model: 'account_managers',
         key: 'am_id',
       }
      });
 
      await queryInterface.changeColumn( 
        'corporate_accounts', 
        'pic_id',
        {
         type : Sequelize.INTEGER,
         references: {
           model: 'pic_corporate_accounts',
           key: 'pic_id',
         }
        });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn('corporate_accounts','am_id',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('corporate_accounts','pic_id',{type : Sequelize.INTEGER});
  }
};

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class billing_detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  billing_detail.init({
    billing_id: DataTypes.INTEGER,
    package_id: DataTypes.INTEGER,
    package_qty: DataTypes.INTEGER,
    package_total: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'billing_detail',
  });
  return billing_detail;
};
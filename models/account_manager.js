'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class account_manager extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.corporate_account, {
        foreignKey: 'am_id',
        as: 'am_has_corp'
      })
    }
  };
  account_manager.init({
    am_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    nik: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    job_position: DataTypes.STRING,
    workplace: DataTypes.STRING,
    avatar: DataTypes.BLOB('long'),
    is_active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'account_manager',
  });
  return account_manager;
};
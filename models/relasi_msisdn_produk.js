'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class relasi_msisdn_produk extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.relasi_msisdn_produk, {
        foreignKey: 'msisdn',
        as: 'relasi_produk_belongsto_msisdn'
      });
    }
  };
  relasi_msisdn_produk.init({
    msisdn: {
      allowNull: false,
      type: DataTypes.STRING
    },
    package_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    start_date: DataTypes.DATE,
    expire_date: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'relasi_msisdn_produk',
  });
  return relasi_msisdn_produk;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order_item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  order_item.init({
    order_id:{
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    msisdn: DataTypes.STRING,
    package_id: DataTypes.INTEGER,
    renewal: DataTypes.BOOLEAN,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'order_item',
  });
  return order_item;
};
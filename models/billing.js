'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class billing extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  billing.init({
    billing_id:  {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    account_id: DataTypes.STRING,
    month: DataTypes.STRING,
    year: DataTypes.STRING,
    total_invoice: DataTypes.INTEGER,
    total_outstanding: DataTypes.INTEGER,
    virtual_account: DataTypes.STRING,
    status_payment: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'billing',
  });
  return billing;
};
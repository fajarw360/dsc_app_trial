'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class corporate_account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
     
      this.belongsTo(models.account_manager, {
        foreignKey: 'am_id',
        as: 'corp_belongto_am'
      });

      this.belongsTo(models.pic_corporate_account, {
        foreignKey: 'pic_id',
        as: 'corp_belongto_pic'
      });

      this.hasMany(models.msisdn, {
        foreignKey: 'account_id',
        as: 'corp_hasMany_msisdn'
      });
    }
  };
  corporate_account.init({
    account_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    account_name: DataTypes.STRING,
    address: DataTypes.STRING,
    line_of_business: DataTypes.STRING,
    phone: DataTypes.STRING,
    pic_id: DataTypes.INTEGER,
    am_id: DataTypes.INTEGER,
    virtual_account: DataTypes.STRING,
    logo: DataTypes.BLOB,
    is_active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'corporate_account',
  });
  return corporate_account;
};